﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class QuitButton : MonoBehaviour
{
    // Start is called before the first frame update
    private Button button;
    private GameObject gameManager;
    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(this.ButtonClicked);
        gameManager = GameObject.Find("GameManager");
    }

    void ButtonClicked()
    {
        //avisamos game manager
        gameManager.GetComponent<GameManager>().restart = true;
        //Cambiamos a la escena del menu
        SceneManager.LoadScene("Menu");
    }
}
