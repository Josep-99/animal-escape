﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowCurrentAnimal : MonoBehaviour
{
    string currentAnimal;
    GameObject saveManager;

    //animales
    GameObject fox;
    GameObject racoon;
    GameObject wolf;
    GameObject deer;
    GameObject here;
    GameObject bear;

    // Start is called before the first frame update
    void Start()
    {
        saveManager = GameObject.Find("SaveManager");
        
        saveManager.GetComponent<SaveManager>().loadData();

        //animales
        fox = GameObject.Find("Fox");
        racoon = GameObject.Find("Racoon");
        wolf = GameObject.Find("Wolf");
        deer = GameObject.Find("Deer");
        here = GameObject.Find("Here");
        bear = GameObject.Find("Bear");
    }

    // Update is called once per frame
    void Update()
    {
        currentAnimal = saveManager.GetComponent<SaveManager>().currentSkin;
        if(currentAnimal.Length < 1) {
            currentAnimal = "fox";
        }
        
        //fox
        if (currentAnimal.Equals("fox")) {
            fox.SetActive(true);
        } 
        else
        {
            fox.SetActive(false);
        }
        //racoon
        if (currentAnimal.Equals("racoon")) {
            racoon.SetActive(true);
        } 
        else
        {
            racoon.SetActive(false);
        }
        //wolf
        if (currentAnimal.Equals("wolf")) {
            wolf.SetActive(true);
        } 
        else
        {
            wolf.SetActive(false);
        }
        //deer
        if (currentAnimal.Equals("deer")) {
            deer.SetActive(true);
        } 
        else
        {
            deer.SetActive(false);
        }
        //here
        if (currentAnimal.Equals("rabbit")) {
            here.SetActive(true);
        } 
        else
        {
            here.SetActive(false);
        }
        //bear
        if (currentAnimal.Equals("bear")) {
            bear.SetActive(true);
        } 
        else
        {
            bear.SetActive(false);
        }
    }
}
