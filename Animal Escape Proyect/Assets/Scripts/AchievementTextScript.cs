﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementTextScript : MonoBehaviour
{
    Text text;
    int coinAmount;
    GameObject saveManager;

    public void updateText() {
        text = GetComponent<Text>();
        saveManager = GameObject.Find("SaveManager");
        coinAmount = saveManager.GetComponent<SaveManager>().achievementTotalCoins;
        text.text = "Mission coins: " + coinAmount.ToString();
    }
}