﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform FollowPlayer;
    private Vector3 offset;

    void Start()
    {
        offset = transform.position - FollowPlayer.position;
    }

    void LateUpdate()
    {
        Vector3 newPosition = new Vector3(transform.position.x, transform.position.y, offset.z + FollowPlayer.position.z);
        transform.position = newPosition;

        // transform.position = Vector3.Lerp(transform.position, newPosition,10*Time.deltaTime);
    }
}

