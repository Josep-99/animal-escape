﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script encargado de generar los diferentes Tiles(Trozos de mapas)

public class LandscapeManager : MonoBehaviour
{
    private GameManager gameManager;

    public float zSpawn = 0;

    //paisaje
    public GameObject[] landScapePrefabs;
    private List<GameObject> activeLs = new List<GameObject>();

    //Distancia de largo de nuestro trozo de mapa
    public float tileLength = 30;

    //Numero de Tiles que se verán en la pantalla
    public int numberOfTiles = 4;

    private Transform playerTransfom;

    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        playerTransfom = GameObject.FindGameObjectWithTag("Player").transform;
        for (int i = 0; i < numberOfTiles; i++)
        {
            SpawnLandscape(0);   
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(gameManager.GetComponent<GameManager>().distanceM > 1000f) {
            //spawn paisaje quemado
            if(playerTransfom.position.z - 35> zSpawn - (numberOfTiles * tileLength))
            {
            SpawnLandscape(1);
            DeleteLs();
            }
        } else {
            //spawn paisaje
            if(playerTransfom.position.z - 35> zSpawn - (numberOfTiles * tileLength))
            {
            SpawnLandscape(0);
            DeleteLs();
            }
        }
        

    }

    //creacion paisaje
        public void SpawnLandscape(int lsIndex)
    {
        GameObject lsSpawned = Instantiate(landScapePrefabs[lsIndex],transform.forward * zSpawn,transform.rotation);
        activeLs.Add(lsSpawned);
        zSpawn += tileLength;
    }

    //Elimincacion paisaje
    public void DeleteLs()
    {
        Destroy(activeLs[0]);
        activeLs.RemoveAt(0);
    }
}
