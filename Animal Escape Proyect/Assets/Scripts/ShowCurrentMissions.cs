﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowCurrentMissions : MonoBehaviour
{
    Text text;
    SaveManager saveManager;
    AchievementManager achievementManager;
    string resultText = "";
    //para ver si quedan misiones fisponibles
    public bool missionsLeft = false;
    //mostrar una vez
    bool show = true;

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
        saveManager = GameObject.Find("SaveManager").GetComponent<SaveManager>();
        achievementManager = GameObject.Find("AchievementManager").GetComponent<AchievementManager>();

        //cargamos info
        saveManager.loadData();

        //distancia
            if (saveManager.currentDistanceAchievement < achievementManager.distanceAchievementArray.GetLength(0)) {
                string textDinstance = "Run " + achievementManager.distanceAchievementArray[saveManager.currentDistanceAchievement, 0].ToString() + " meters: " +
                achievementManager.distanceAchievementArray[saveManager.currentDistanceAchievement, 1].ToString() + " coins.";
                resultText += textDinstance + "\n";
                missionsLeft = true;
            }

            //monedas
            if (saveManager.currentGameCoinAchievement < achievementManager.gameCoinAchievementArray.GetLength(0)) {
                string textCoin = "Take " + achievementManager.gameCoinAchievementArray[saveManager.currentGameCoinAchievement, 0].ToString() + " coins: " +
                achievementManager.gameCoinAchievementArray[saveManager.currentGameCoinAchievement, 1].ToString() + " coins.";
                resultText += textCoin + "\n";
                missionsLeft = true;
            }

            //saltos
            if (saveManager.currentJumpAchievement < achievementManager.jumpAchievementArray.GetLength(0)) {
                string textJump = "Jump " + achievementManager.jumpAchievementArray[saveManager.currentJumpAchievement, 0].ToString() + " times: " +
                achievementManager.jumpAchievementArray[saveManager.currentJumpAchievement, 1].ToString() + " coins.";
                resultText += textJump + "\n";
                missionsLeft = true;
            }

            //slide
            if (saveManager.currentSlideAchievement < achievementManager.slideAchievementArray.GetLength(0)) {
                string textSlide = "Slide " + achievementManager.slideAchievementArray[saveManager.currentSlideAchievement, 0].ToString() + " times: " +
                achievementManager.slideAchievementArray[saveManager.currentSlideAchievement, 1].ToString() + " coins.";
                resultText += textSlide + "\n";
                missionsLeft = true;
            }

            //revivir
            if (saveManager.currentReviveAchievement < achievementManager.reviveAchievementArray.GetLength(0)) {
                string textRevive;
                if (saveManager.currentReviveAchievement == 0) {
                    textRevive = "Revive " + achievementManager.reviveAchievementArray[saveManager.currentReviveAchievement, 0].ToString() + " time: " +
                    achievementManager.reviveAchievementArray[saveManager.currentReviveAchievement, 1].ToString() + " coins.";
                } else {
                    textRevive = "Revive " + achievementManager.reviveAchievementArray[saveManager.currentReviveAchievement, 0].ToString() + " times: " +
                    achievementManager.reviveAchievementArray[saveManager.currentReviveAchievement, 1].ToString() + " coins.";
                }
                
                resultText += textRevive + "\n";
                missionsLeft = true;
            }

            //sin monedas
            if (saveManager.noCoinGameAchievementComplete == 0) {
                resultText += "Don't take any coins in a run: " + achievementManager.noCoinReward.ToString();
                missionsLeft = true;
            }

            //si no quedan misiones
            if (!missionsLeft) {
                resultText += "\nNo missions available";
            }

            text.text = resultText;
    }
}
/*
    REWARDS

    private int[,] distanceAchievementArray = new int[,]{{150, 50}, {300, 100}, {600, 200}, {900, 300}, {1200, 400}};
    private int[,] gameCoinAchievementArray = new int[,]{{50, 20}, {100, 40}, {150, 60}, {200, 80}, {250, 100}, {300, 120}};
    private int[,] jumpAchievementArray = new int[,]{{20, 15}, {40, 50}, {60, 80}, {80, 120}};
    private int[,] slideAchievementArray = new int[,]{{5, 10}, {10, 25}, {20, 75}, {35, 150}};
    private int[,] reviveAchievementArray = new int[,]{{1, 150}, {2, 250}, {3, 350}, {4, 450}};
    private int noCoinReward = 100;
*/
