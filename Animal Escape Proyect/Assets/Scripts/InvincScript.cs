﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvincScript : MonoBehaviour
{
    GameObject GameManager;
    GameManager gmanager;

    void Start()
    {
        GameManager = GameObject.Find("GameManager");
        gmanager = GameManager.GetComponent<GameManager>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player") {
            FindObjectOfType<SoundManager>().PlaySound("guindilla");

            Debug.Log("Powerup de invencibilidad");
            gmanager.isInvenciblePowerUpTakeIt = true;
            Destroy(gameObject);
        }
    }
}
