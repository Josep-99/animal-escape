﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class ContinueButton : MonoBehaviour
{
    private Button button;
    private GameObject gameManager;
    public GameObject menu;
    public bool pause = true;
    PlayerController pcontroller;
    GameObject Player;
    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(this.ButtonClicked);
        gameManager = GameObject.Find("GameManager");
        Player = GameObject.Find("Player");
        pcontroller = Player.GetComponent<PlayerController>();
    }
    void Update()
    {
        if (pause)
        {
            Debug.Log("dsmoamdoamdao");
            Time.timeScale = 0f;
        }
    }
    void ButtonClicked()
    {
        //avisamos game manager
        menu.SetActive(false);
       // gameManager.GetComponent<GameManager>().isPause = false;
        pause = true;
        pcontroller.gameIsPaused = false;
    }
}