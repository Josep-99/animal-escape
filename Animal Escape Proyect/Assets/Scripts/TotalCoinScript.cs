﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TotalCoinScript : MonoBehaviour
{
    Text coinText;
    int coinAmount;
    GameObject saveManager;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void showTotal() {
        coinText = GetComponent<Text>();
        saveManager = GameObject.Find("SaveManager");
        coinAmount = saveManager.GetComponent<SaveManager>().getTotalCoins();
        coinText.text = "Total coins: " + coinAmount.ToString();
    }
}