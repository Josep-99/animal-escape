﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private CharacterController characterController;
    private Vector3 direction;
    public float forwardspeed;
    private int carril = 1;  // 0 izquierda 1 medio 2 derecha;
    public float distanciaEntreCarril = 4;
    public float jumpForce;
    public float Gravity = -20;
    public float jumpSlideGravity = -75;
    public bool slideOnGroundTouch = false;
    public bool isInvencible;
    private Vector3 lastPos;
    private GameObject gameManager;
    GameManager gmanager;

    public bool slideFalling = false;

    //booleanos para animaciones
    public bool isSliding = false;
    public bool isJumping = false;
    public bool isRunning = true;
    public bool isBurned = false;
    public bool swipeLeft = false;
    public bool swipeRight = false;

    //cooldown slide
    private float coolDownRemaining;
    private float coolDownTime = 1f;

    //gameobject que se choca
    public GameObject obstaculoChoque;

    //Variable para misiones
    private GameObject achievementManager;
    private int jumpCount = 0;
    private int slideCount = 0;
    private bool jumpIsDetectable = false;
    private bool slideIsDetectable = false;
    private bool slideDetected = false;
    private GameObject saveManager;

    //variables del character controller
    private float heightCharController;
    private Vector3 centerCharController;

    //Variable que comprueba si el juego está pausado
    public bool gameIsPaused = false;

    void Start()
    {
        gameManager = GameObject.Find("GameManager");
        gmanager = gameManager.GetComponent<GameManager>();
        characterController = GetComponent<CharacterController>();
        lastPos = transform.position;
        //forwardspeed = gameManager.GetComponent<GameManager>().speed;
        achievementManager = GameObject.Find("AchievementManager");
        saveManager = GameObject.Find("SaveManager");
        //variables del character controller
        heightCharController = characterController.height;
        centerCharController = new Vector3(0f, 0.04f, 0.7f);

        coolDownRemaining = coolDownTime;
    }

    void Update()
    {
        isInvencible = gmanager.isInvencibleBool;

        if (!isBurned && !gameIsPaused)
        {
            //recogemos la velocidad
            /*forwardspeed = gameManager.GetComponent<GameManager>().speed;

            direction.z = forwardspeed;*/
            //
            if (!slideFalling)
            {
                direction.y += Gravity * Time.deltaTime;
            }
            if (slideFalling)
            {
                direction.y += jumpSlideGravity * Time.deltaTime;
            }

            ///
            if (characterController.isGrounded)
            {
                isJumping = false;
                slideFalling = false;
                //direction.y = -1;
                if (SwipeController.swipeUp)
                {
                    Jump();
                }
                if (SwipeController.swipeDown && !isSliding)
                {
                    isSliding = true;
                    Debug.Log(isSliding);
                }

            }
            if (!characterController.isGrounded && SwipeController.swipeDown)
            {
                slideFalling = true;
                slideOnGroundTouch = true;
                isSliding = true;
            }
        }
        else
        {
            direction.y += Gravity * Time.deltaTime;

        }


        if (!gameIsPaused)
        {
            if (characterController.isGrounded && slideOnGroundTouch)
            {
                slideOnGroundTouch = false;
                Slide();
            }

            if (isSliding && !slideOnGroundTouch)
            {
                Slide();
            }

            if (SwipeController.swipeRight && !isBurned)
            {

                carril++;
                if (carril == 3)
                {
                    carril = 2;
                }

                swipeRight = true;
            }
            if (SwipeController.swipeLeft && !isBurned)
            {
                carril--;
                if (carril == -1)
                {
                    carril = 0;
                }

                swipeLeft = true;
            }

            Vector3 targetPosition = transform.position.z * transform.forward + transform.position.y * transform.up;
            if (carril == 0)
            {
                targetPosition += Vector3.left * distanciaEntreCarril;
            }
            else if (carril == 2)
            {
                targetPosition += Vector3.right * distanciaEntreCarril;
            }


            // transform.position = Vector3.Lerp(transform.position,targetPosition,80*Time.deltaTime);
            
            //hace el cambio de carril repentino
            //transform.position = targetPosition;
            //characterController.center = characterController.center;
            
            //cambio de carril suave
            if (transform.position != targetPosition) {
                CenterDisplacement(targetPosition);
            } else {
                swipeLeft = false;
                swipeRight = false;
            }

            //check si se desplaza
            isMoving();

            if (jumpIsDetectable)
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, Vector3.down, out hit, 10.0f))
                {
                    if (hit.transform.tag == "Obstacles")
                    {
                        jumpIsDetectable = false;
                        jumpCount++;
                        achievementManager.GetComponent<AchievementManager>().checkIfObstacleJumpedAchievement(saveManager.GetComponent<SaveManager>().currentJumpAchievement, jumpCount);
                    }
                }
            }

            if (slideIsDetectable)
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, Vector3.up, out hit, 5.0f))
                {
                    if (hit.transform.tag == "Obstacles" && isSliding)
                    {
                        slideIsDetectable = false;
                        slideDetected = true;
                        slideCount++;
                        achievementManager.GetComponent<AchievementManager>().checkIfObstacleSlideAchievement(saveManager.GetComponent<SaveManager>().currentSlideAchievement, slideCount);
                    }
                }
            }
        }

    }
    private void FixedUpdate()
    {
        direction.z = forwardspeed;
        characterController.Move(direction * Time.deltaTime);
    }

    //desplazamiento centro cambio de carril
    private void CenterDisplacement(Vector3 targetPosition) {
        //diferencia 
        Vector3 difference = targetPosition - transform.position;
        //desplazamiento
        Vector3 movementDir = difference.normalized * 25 * Time.deltaTime; 
        if (movementDir.sqrMagnitude < difference.sqrMagnitude) {
            characterController.Move(movementDir);
        } else {
            characterController.Move(difference);
        }
    }

    private void Jump()
    {
        isSliding = false;
        //ponemos los valores predeterminados del character controller
        characterController.center = centerCharController;
        characterController.height = heightCharController;
        isJumping = true;
        direction.y = jumpForce;
        jumpIsDetectable = true;
    }
    private void Slide()
    {
        characterController.center = new Vector3(0, -0.5f, 0);
        characterController.height = 1;
        canDetectSlide();
        // animator.SetBool("isSliding", true);
        if (coolDownRemaining <= 0)
        {
            characterController.center = centerCharController;
            characterController.height = heightCharController;
            // animator.SetBool("isSliding", false);
            isSliding = false;
            slideDetected = false;

            coolDownRemaining = coolDownTime;
        }
        coolDownRemaining -= Time.deltaTime;
    }

    private void canDetectSlide()
    {
        if (isSliding && slideIsDetectable == false && slideDetected == false)
        {
            slideIsDetectable = true;
        }
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (isInvencible)
        {
            if (hit.transform.tag == "Obstacles" || hit.transform.tag == "Tree")
            {
                Destroy(hit.gameObject);

            }
            if (hit.transform.tag == "Fire")
            {
                Destroy(hit.gameObject);
            }
        }
        else
        {
            if (hit.transform.tag == "Obstacles" || hit.transform.tag == "Tree")
            {
                isRunning = false;
                obstaculoChoque = hit.gameObject;
            }

            if (hit.transform.tag == "Fire")
            {
                isBurned = true;
                obstaculoChoque = hit.gameObject;
            }
        }
    }

    private void isMoving()
    {
        Vector3 displacement = transform.position - lastPos;
        lastPos = transform.position;

        if (displacement.magnitude > 0.001)
        {
            isRunning = true;
        }
    }
}

