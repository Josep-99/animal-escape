﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class PauseButton : MonoBehaviour
{
    private Button button;
    private GameObject gameManager;
    public GameObject menu;
    PlayerController pcontroller;
    GameObject Player;

    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(this.ButtonClicked);
        gameManager = GameObject.Find("GameManager");
        Player = GameObject.Find("Player");
        pcontroller = Player.GetComponent<PlayerController>();
    }


    void ButtonClicked()
    {
        //avisamos game manager
        pcontroller.gameIsPaused = true;
        Debug.Log(pcontroller.gameIsPaused);
        menu.SetActive(true);
        pcontroller.gameIsPaused = true;
       // gameManager.GetComponent<GameManager>().isPause = true;
    }
}
