﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetScript : MonoBehaviour
{
    GameObject GameManager;
    GameManager gmanager;

    void Start()
    {
        GameManager = GameObject.Find("GameManager");
        gmanager = GameManager.GetComponent<GameManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player") {
            FindObjectOfType<SoundManager>().PlaySound("magneticEffect");

            Debug.Log("Powerup de imán");
            gmanager.isMagneticPowerUpTakeIt = true;
            Destroy(gameObject);
        }
    }
}
