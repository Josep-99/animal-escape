﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DistanceAmount : MonoBehaviour
{
    int distanceAmount = 0;
    Text distanceText;
    GameObject GameManager;
    GameManager gmanager;

    void Start()
    {
        distanceText = GetComponent<Text>();
        GameManager = GameObject.Find("GameManager");
        gmanager = GameManager.GetComponent<GameManager>();
    }

    void Update()
    {
        distanceAmount = (int)gmanager.distanceM;
        updateDistance();
    }
    public void updateDistance()
    {
        /*if (distanceAmount.ToString().Length < 6)
        {
        }
        else
        {
            distanceText.text = distanceAmount.ToString().Remove(6) + " m";
        }*/
        distanceText.text = distanceAmount.ToString() + " m";
    }
}
