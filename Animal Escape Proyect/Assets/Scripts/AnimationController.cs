﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    private PlayerController player;
    private Animator _anim;

    // Start is called before the first frame update
    void Start()
    {
        player = transform.parent.GetComponent<PlayerController>();
        _anim = GetComponent<Animator>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //jump
        if (player.isJumping)
        {
            _anim.SetBool("isJumping", true);
        } 
        else
        {
            _anim.SetBool("isJumping", false);
        }
        //slide
        if (player.isSliding) {
            _anim.SetBool("isSlidding", true);
        } 
        else
        {
            _anim.SetBool("isSlidding", false);
        }
        //hit
         if (player.isRunning) {
            _anim.SetBool("isRunning", true);
        } 
        else
        {
            _anim.SetBool("isRunning", false);
        }
        //burned
        if (player.isBurned) {
            _anim.SetBool("isBurned", true);
        } 
        else
        {
            _anim.SetBool("isBurned", false);
        }
        //swipe left and right
        if (player.swipeLeft) {
            this.transform.rotation = Quaternion.Euler(0, -10, 0);
        }else if (player.swipeRight) {
            this.transform.rotation = Quaternion.Euler(0, 10, 0);
        } else {
            this.transform.rotation = Quaternion.Euler(0, 0, 0);
        }
    }
}
