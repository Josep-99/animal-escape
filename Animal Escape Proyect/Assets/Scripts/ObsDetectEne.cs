﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObsDetectEne : MonoBehaviour
{

    public LayerMask layer;
    public GameObject enemyObject;
    public GameObject particleSystem;

    private void Start()
    {
        enemyObject = GameObject.Find("Cube");
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.back), out hit, 1.7f, layer) || Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 1.7f, layer)
             || Physics.Raycast(transform.position, transform.TransformDirection(Vector3.left), out hit, 1.7f, layer) || Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), out hit, 1.7f, layer)
              || Physics.Raycast(transform.position, transform.TransformDirection(Vector3.up), out hit, 1.7f, layer) || Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, 1.7f, layer))
        {
            if(hit.transform.gameObject.tag == "Enemy")
            {
                if (this.gameObject.tag == "Tree")
                {
                    particleSystem = GameObject.Find("ParticleTree");
                    particleSystem.GetComponent<ParticleSystem>().Play();
                    FindObjectOfType<SoundManager>().PlaySound("TreeCut");
                }
                Destroy(this.gameObject);
            }
        }
    }
}
