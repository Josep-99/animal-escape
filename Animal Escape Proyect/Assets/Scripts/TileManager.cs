﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script encargado de generar los diferentes Tiles(Trozos de mapas)

public class TileManager : MonoBehaviour
{
    /*
     * En esta clase guardaremos los diferentes trozos de mapas que hayamos creado
     * previamente en la carpeta Prefabs
     */
    public GameObject[] tilesPrefabs;
    public GameObject[] fireTilesPrefabs;
    public GameObject[] moreFireTilesPrefabs;
    private GameObject[] allTiles;
    private GameObject[] allFireTiles;
    public float zSpawn = 0;

    //tile anterior
    private int lastTile;

    //Distancia de largo de nuestro trozo de mapa
    public float tileLength = 30;

    //Numero de Tiles que se verán en la pantalla
    public int numberOfTiles = 3;

    private List<GameObject> activeTiles = new List<GameObject>();

    public Transform playerTransfom;

    private GameObject gameManager;

    //contador para controlar tiles de fuego
    private int contDifficultLevel = 0;

    void Start()
    {
        gameManager = GameObject.Find("GameManager");

        //Cargamos todas las tiles en un solo array
        allTiles = new GameObject[tilesPrefabs.Length + fireTilesPrefabs.Length];
        int h = 0;
        for(int j = 0; j < tilesPrefabs.Length; j++) {
            allTiles[j] = tilesPrefabs[j];
        }
        for(int j = tilesPrefabs.Length; j < allTiles.Length; j++) {
            allTiles[j] = fireTilesPrefabs[h];
            h++;
        }

        //Cargamos todas las tiles de fuego en un solo array
        allFireTiles = new GameObject[fireTilesPrefabs.Length + moreFireTilesPrefabs.Length];
        h = 0;
        for(int j = 0; j < fireTilesPrefabs.Length; j++) {
            allFireTiles[j] = fireTilesPrefabs[j];
        }
        for(int j = fireTilesPrefabs.Length; j < allFireTiles.Length; j++) {
            allFireTiles[j] = moreFireTilesPrefabs[h];
            h++;
        }

        //Creacion de los diferentes Tiles de manera aleatoria
        for (int i = 0; i < numberOfTiles; i++)
        {
            //Para evitar conflictos al comenzar, el primer Tile generado no tendrá obstaculos
            if (i == 0)
            {
                SpawnTile(tilesPrefabs ,0);
            }
            else
            {
                SpawnTile(tilesPrefabs, Random.Range(1, tilesPrefabs.Length));
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(gameManager.GetComponent<GameManager>().distanceM > 1000f) {
            //aumenta la dificultad, 5 tiles de fuego - 3 sin fuego
            if(playerTransfom.position.z - 35> zSpawn - (numberOfTiles * tileLength))
            {
                Debug.Log(contDifficultLevel);
                if (contDifficultLevel < 5) {
                    SpawnTile(allFireTiles ,Random.Range(0, allFireTiles.Length));
                    contDifficultLevel++;
                } else if (contDifficultLevel <= 8) {
                    SpawnTile(tilesPrefabs ,RandomSpawnController(tilesPrefabs));
                    if (contDifficultLevel == 8) {
                        contDifficultLevel = 0;
                    } else {
                        contDifficultLevel++;
                    }
                }
                DeleteTile();
            }
        } else {
            if(gameManager.GetComponent<GameManager>().distanceM > 500f) {
                //pueden aparcer obstaculos de fuego
                Debug.Log("Fuego puede empezar a aparecer");
                if(playerTransfom.position.z - 35> zSpawn - (numberOfTiles * tileLength))
                {
                    SpawnTile(allTiles ,RandomSpawnController(allTiles));
                    DeleteTile();
                }
            } else {
                if(playerTransfom.position.z - 35> zSpawn - (numberOfTiles * tileLength))
                {
                    //random al inicio sin fuego
                    SpawnTile(tilesPrefabs ,RandomSpawnController(tilesPrefabs));
                    DeleteTile();
                }
            }
        }
    }

    //Metodo que comprueba si el ultimo tile está bacio para que no vuelva a salir
    private int RandomSpawnController(GameObject [] array) {
        int randomNum = Random.Range(0, array.Length);
        if (lastTile == 0 && randomNum == 0) {
            return RandomSpawnController(array);
        } else {
            lastTile = randomNum;
            return randomNum;
        }
    }

    //Creacion de los Tiles
    public void SpawnTile(GameObject[] tileArray, int tileIndex)
    {
        GameObject tileSpawned = Instantiate(tileArray[tileIndex],transform.forward * zSpawn,transform.rotation);
        int randomNumber = Random.Range(1, 24);
        GameObject coinLayout1 = tileSpawned.transform.Find("Layout1").gameObject;
        GameObject coinLayout2 = tileSpawned.transform.Find("Layout2").gameObject;
        GameObject powerLayout1 = tileSpawned.transform.Find("Layout3").gameObject;
        GameObject powerLayout2 = tileSpawned.transform.Find("Layout4").gameObject;
        GameObject powerLayout3 = tileSpawned.transform.Find("Layout5").gameObject;
        if(randomNumber >= 1 && randomNumber <= 9) {
            //Coin layout 1 is the one used
            coinLayout1.SetActive(true);
            coinLayout2.SetActive(false);
            powerLayout1.SetActive(false);
            powerLayout2.SetActive(false);
            powerLayout3.SetActive(false);
        } else if(randomNumber >= 10 && randomNumber <= 18) {
            //Coin layout 2 is the one used
            coinLayout1.SetActive(false);
            coinLayout2.SetActive(true);
            powerLayout1.SetActive(false);
            powerLayout2.SetActive(false);
            powerLayout3.SetActive(false);
        } else if(randomNumber == 19){
            //Powerup appears
            coinLayout1.SetActive(false);
            coinLayout2.SetActive(false);
            powerLayout1.SetActive(true);
            powerLayout2.SetActive(false);
            powerLayout3.SetActive(false);
        } else if(randomNumber == 20){
            //Powerup appears
            coinLayout1.SetActive(false);
            coinLayout2.SetActive(false);
            powerLayout1.SetActive(false);
            powerLayout2.SetActive(true);
            powerLayout3.SetActive(false);
        } else if(randomNumber == 21){
            //Powerup appears
            coinLayout1.SetActive(false);
            coinLayout2.SetActive(false);
            powerLayout1.SetActive(false);
            powerLayout2.SetActive(false);
            powerLayout3.SetActive(true);
        } else if(randomNumber >= 22 || randomNumber <= 24) {
            coinLayout1.SetActive(false);
            coinLayout2.SetActive(false);
            powerLayout1.SetActive(false);
            powerLayout2.SetActive(false);
            powerLayout3.SetActive(false);
        }
        activeTiles.Add(tileSpawned);
        zSpawn += tileLength;

    }
    //Elimincacion de los Tiles
    public void DeleteTile()
    {
        Destroy(activeTiles[0]);
        activeTiles.RemoveAt(0);
    }
}
