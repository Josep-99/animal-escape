﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ShopController : MonoBehaviour
{
    // Start is called before the first frame update
    private Button button;
    private GameObject gameManager;
    private int WolfPrice = 100;
    private int RacoonPrice = 500;
    private int HerePrice = 1000;
    private int DeerPrice = 1500;
    private int BearPrice = 0;
    private SaveManager saveManager;
    AchievementManager achievementManager;
    ShowCurrentMissions showCurrentMissions;
    //wolf
    private GameObject wolfBuyButton;
    private GameObject wolfEquipButton;
    //racoon
    private GameObject racoonBuyButton;
    private GameObject racoonEquipButton;
    //fox
    private GameObject foxEquipButton;
    //here
    private GameObject hereBuyButton;
    private GameObject hereEquipButton;
    //deer
    private GameObject deerBuyButton;
    private GameObject deerEquipButton;
    //bear
    private GameObject bearBuyButton;
    private GameObject bearEquipButton;

    public GameObject backButton;

    //texto dinero
    public Text currentMoney;

    //texto de precio animales
    public Text foxPriceText;
    public Text wolfPriceText;
    public Text racoonPriceText;
    public Text herePriceText;
    public Text deerPriceText;
    public Text bearPriceText;
    
    //animal equipado
    private Text equipedAnimalPrice;
    private GameObject equipedAnimalButton;

    //strings
    private string equiped = "Equiped";
    private string owned = "Owned";

    // Start is called before the first frame update
    void Start()
    {
        saveManager = GameObject.Find("SaveManager").GetComponent<SaveManager>();
        achievementManager = GameObject.Find("AchievementManager").GetComponent<AchievementManager>();
        showCurrentMissions = GameObject.Find("Missions").GetComponent<ShowCurrentMissions>();
        //buttons
        //lobo
        wolfBuyButton = GameObject.Find("WolfBuyButton");
        wolfEquipButton = GameObject.Find("WolfEquipButton");
        //mapache
        racoonBuyButton = GameObject.Find("RacoonBuyButton");
        racoonEquipButton = GameObject.Find("RacoonEquipButton");
        //zorro
        foxEquipButton = GameObject.Find("FoxEquipButton");
        //liebre
        hereBuyButton = GameObject.Find("HereBuyButton");
        hereEquipButton = GameObject.Find("HereEquipButton");
        //ciervo
        deerBuyButton = GameObject.Find("DeerBuyButton");
        deerEquipButton = GameObject.Find("DeerEquipButton");
        //oso
        bearBuyButton = GameObject.Find("BearBuyButton");
        bearEquipButton = GameObject.Find("BearEquipButton");

        //mostrar botones correctos segun el estado de cada animal
        UpdateShop();
    }


    public void backToMenu() 
    {
        GameObject.Find("ShopCanvas").SetActive(false);
    }
    //FOX
    public void equipFox() 
    {
        FindObjectOfType<SoundManager>().PlaySound("fox");

        saveManager.equipAnimal("fox");
        //cambiamos los valores del animal que estaba equipado y guardamos el actual
        equipedAnimalPrice.text = owned;
        equipedAnimalButton.SetActive(true);
        UpdateShop();
    }
    //WOLF
    public void buyWolf()
    {
        if(saveManager.wolfOwned == 0) {
            if (saveManager.coinCount>=WolfPrice)
            {
                saveManager.substractCoins(WolfPrice);
                saveManager.purchasedSkin("wolf");            
                wolfBuyButton.SetActive(false);
                wolfEquipButton.SetActive(true);
                saveManager.wolfOwned = 1;
                UpdateShop();
            }
        }
    }
    public void equipWolf() 
    {
        FindObjectOfType<SoundManager>().PlaySound("wolf");
        saveManager.equipAnimal("wolf");
        saveManager.currentSkin = "wolf";
        //cambiamos los valores del animal que estaba equipado y guardamos el actual
        equipedAnimalPrice.text = owned;
        equipedAnimalButton.SetActive(true);
        UpdateShop();
    }
    //RACOON
    public void buyRacoon() 
    {
        if(saveManager.racoonOwned == 0) {
            if (saveManager.coinCount >= RacoonPrice)
            {
                saveManager.substractCoins(RacoonPrice);
                saveManager.purchasedSkin("racoon");
                racoonBuyButton.SetActive(false);
                racoonEquipButton.SetActive(true);
                saveManager.racoonOwned = 1;
                UpdateShop();
            }
        }  
    }

    public void equipRacoon() 
    {
        FindObjectOfType<SoundManager>().PlaySound("racoon");

        saveManager.equipAnimal("racoon");
        saveManager.currentSkin = "racoon";
        //cambiamos los valores del animal que estaba equipado y guardamos el actual
        equipedAnimalPrice.text = owned;
        equipedAnimalButton.SetActive(true);
        UpdateShop();
    }
    //HERE/RABBIT
    public void buyHere()
    {
        if(saveManager.rabbitOwned == 0) {
            if (saveManager.coinCount>=HerePrice)
            {
                saveManager.substractCoins(HerePrice);
                saveManager.purchasedSkin("rabbit");            
                hereBuyButton.SetActive(false);
                hereEquipButton.SetActive(true);
                saveManager.rabbitOwned = 1;
                UpdateShop();
            }
        }
    }
    public void equipHere() 
    {
        FindObjectOfType<SoundManager>().PlaySound("rabbit");

        saveManager.equipAnimal("rabbit");
        saveManager.currentSkin = "rabbit";
        //cambiamos los valores del animal que estaba equipado y guardamos el actual
        equipedAnimalPrice.text = owned;
        equipedAnimalButton.SetActive(true);
        UpdateShop();
    }
    
    //DEER
    public void buyDeer()
    {
        if(saveManager.deerOwned == 0) {
            if (saveManager.coinCount>=DeerPrice)
            {
                saveManager.substractCoins(DeerPrice);
                saveManager.purchasedSkin("deer");            
                deerBuyButton.SetActive(false);
                deerEquipButton.SetActive(true);
                saveManager.deerOwned = 1;
                UpdateShop();
            }
        }
    }
    public void equipDeer() 
    {
        FindObjectOfType<SoundManager>().PlaySound("deer");

        saveManager.equipAnimal("deer");
        saveManager.currentSkin = "deer";
        //cambiamos los valores del animal que estaba equipado y guardamos el actual
        equipedAnimalPrice.text = owned;
        equipedAnimalButton.SetActive(true);
        UpdateShop();
    }
    //BEAR
    public void buyBear()
    {
        if(saveManager.bearOwned == 0) {
            if (saveManager.coinCount>=BearPrice)
            {

                //saveManager.substractCoins(BearPrice);
                saveManager.purchasedSkin("bear");            
                bearBuyButton.SetActive(false);
                bearEquipButton.SetActive(true);
                saveManager.bearOwned = 1;
                UpdateShop();
            }
        }
    }
    public void equipBear() 
    {
        FindObjectOfType<SoundManager>().PlaySound("bear");

        saveManager.equipAnimal("bear");
        saveManager.currentSkin = "bear";
        //cambiamos los valores del animal que estaba equipado y guardamos el actual
        equipedAnimalPrice.text = owned;
        equipedAnimalButton.SetActive(true);
        UpdateShop();
    }

    //actualizar tienda
    void UpdateShop() {
        currentMoney.text = saveManager.coinCount.ToString();

        //fox
        if (saveManager.currentSkin.Equals("fox")) {
                foxPriceText.text = equiped;
                foxEquipButton.SetActive(false);

                equipedAnimalPrice = foxPriceText;
                equipedAnimalButton = foxEquipButton; 
            } else {
                foxPriceText.text = owned;
            }

        //wolf
        if (saveManager.wolfOwned == 1)
        {
            wolfBuyButton.SetActive(false);
            if (saveManager.currentSkin.Equals("wolf")) {
                wolfPriceText.text = equiped;
                wolfEquipButton.SetActive(false);

                equipedAnimalPrice = wolfPriceText;
                equipedAnimalButton = wolfEquipButton; 
            } else {
                wolfPriceText.text = owned;
            }
        }
        else 
        {
            wolfEquipButton.SetActive(false);   
        }

        //racoon
        if (saveManager.racoonOwned == 1)
        {
            racoonBuyButton.SetActive(false);
            if (saveManager.currentSkin.Equals("racoon")) {
                racoonPriceText.text = equiped;
                racoonEquipButton.SetActive(false);

                equipedAnimalPrice = racoonPriceText;
                equipedAnimalButton = racoonEquipButton; 
            } else {
                racoonPriceText.text = owned;
            }
        }
        else
        {
            racoonEquipButton.SetActive(false);
        }
        //here
        if (saveManager.rabbitOwned == 1)
        {
            hereBuyButton.SetActive(false);
            if (saveManager.currentSkin.Equals("rabbit")) {
                herePriceText.text = equiped;
                hereEquipButton.SetActive(false);

                equipedAnimalPrice = herePriceText;
                equipedAnimalButton = hereEquipButton; 
            } else {
                herePriceText.text = owned;
            }
        }
        else
        {
            hereEquipButton.SetActive(false);
        }
        //deer
        if (saveManager.deerOwned == 1)
        {
            deerBuyButton.SetActive(false);
            if (saveManager.currentSkin.Equals("deer")) {
                deerPriceText.text = equiped;
                deerEquipButton.SetActive(false);

                equipedAnimalPrice = deerPriceText;
                equipedAnimalButton = deerEquipButton; 
            } else {
                deerPriceText.text = owned;
            }
        }
        else
        {
            deerEquipButton.SetActive(false);
        }
        //bear
        if (showCurrentMissions.missionsLeft)
        {
            bearBuyButton.SetActive(false);
            bearEquipButton.SetActive(false);
        }
        else
        {
            if (saveManager.currentSkin.Equals("bear")) {
                bearPriceText.fontSize = 55;
                bearPriceText.text = equiped;
                bearEquipButton.SetActive(false);
                bearBuyButton.SetActive(false);

                equipedAnimalPrice = bearPriceText;
                equipedAnimalButton = bearEquipButton; 
            } else if (saveManager.bearOwned == 1){
                bearBuyButton.SetActive(false);
                bearPriceText.fontSize = 55;
                bearPriceText.text = owned;
            } else {
                bearEquipButton.SetActive(false);
            }
        }
    }
}

