﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemicFollow : MonoBehaviour
{
    private CharacterController enemic;
    private Vector3 direction;
    public float forwardSpeed;
    public Canvas deathScreen1;
    private GameObject gameManager;
    private GameObject player;
    private Vector3 originalVector;
    private float currentZ;
    public ParticleSystem treeParticule;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("GameManager");
        player = GameObject.Find("Player");
        enemic = GetComponent<CharacterController>();
        enemic.detectCollisions = false;
        originalVector = this.gameObject.transform.position;
        currentZ = originalVector.z;
    }

    // Update is called once per frame
    /*void Update()
    {
        forwardSpeed = gameManager.GetComponent<GameManager>().speed;
        direction.z = forwardSpeed;
    }*/
    private void FixedUpdate()
    {
        currentZ = this.gameObject.transform.position.z;
        this.gameObject.transform.position = new Vector3(originalVector.x, originalVector.y, currentZ);
        direction.z = forwardSpeed;
        direction.y = -0.5f;
        if (gameManager.GetComponent<GameManager>().inLife) {
            enemic.Move(direction * Time.fixedDeltaTime);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            deathScreen1.gameObject.SetActive(true);
            gameManager.GetComponent<GameManager>().inLife = false;
        }
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.gameObject.tag == "Tree") 
        {
            treeParticule.Play();
            FindObjectOfType<SoundManager>().PlaySound("TreeCut");

            Destroy(hit.gameObject);
        }

        if (hit.gameObject.tag == "Obstacles")
        {
           
            Destroy(hit.gameObject);
        }

        if (hit.gameObject.tag == "Fire") {
            Destroy(hit.gameObject);
        }
    }
}
