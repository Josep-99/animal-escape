﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContinueScript : MonoBehaviour
{
    private Button button;
    private GameObject saveManager;
    private int revivePrice;
    private GameObject gameManager;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("GameManager");

        button = GetComponent<Button>();
        button.onClick.AddListener(this.ButtonClicked);
        saveManager = GameObject.Find("SaveManager");
        revivePrice = gameManager.GetComponent<GameManager>().revivePrice;
    }

    void update() {
        if (saveManager.GetComponent<SaveManager>().getTotalCoins() > revivePrice) {
            button.GetComponent<Image>().color = Color.white;
        } else {
            button.GetComponent<Image>().color = Color.red;
        }
    }

    void ButtonClicked()
    {
        if(saveManager.GetComponent<SaveManager>().getTotalCoins() > revivePrice) {
            Debug.Log("Se puede revivir");
            Debug.Log(saveManager.GetComponent<SaveManager>().getTotalCoins());
            gameManager.GetComponent<GameManager>().pauseFrame = 0;
            gameManager.GetComponent<GameManager>().revive = true;
        } else {
            Debug.Log("No se puede revivir");
        }
    }

    void updatePrice() {
        //TODO: Aumentar precio de revivir cada vez que se usa
    }
}

