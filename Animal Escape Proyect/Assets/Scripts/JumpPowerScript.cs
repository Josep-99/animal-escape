﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpPowerScript : MonoBehaviour
{
    GameObject GameManager;
    GameManager gmanager;

    void Start()
    {
        GameManager = GameObject.Find("GameManager");
        gmanager = GameManager.GetComponent<GameManager>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player") {
            FindObjectOfType<SoundManager>().PlaySound("jump");

            Debug.Log("Powerup de salto");
            gmanager.isJumpPowerUpTakeIt = true;
            Destroy(gameObject);
        }
    }
}
