﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class PowerupUIScript : MonoBehaviour
{
    public Color fullColor;
    public Color emptyColor;
    public Slider firstSlider;
    public Slider secondSlider;
    public Slider thirdSlider;
    public GameObject GameManager;
    public GameManager gmanager;
    public Text firstText;
    public Text secondText;
    public Text thirdText;

    // Start is called before the first frame update
    void Start()
    {
        //slider = GameObject.Find("FirstSlider");
        firstSlider = GameObject.Find("FirstSlider").GetComponent<Slider>();
        secondSlider = GameObject.Find("SecondSlider").GetComponent<Slider>();
        thirdSlider = GameObject.Find("ThirdSlider").GetComponent<Slider>();
        GameManager = GameObject.Find("GameManager");
        gmanager = GameManager.GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if(gmanager.sliderID[0] > 0f) {

            firstSlider.gameObject.SetActive(true);
            firstSlider.value = gmanager.timerCalculation(gmanager.sliderID[0], 0);
            firstSlider.gameObject.transform.Find("Fill Area").Find("Fill").GetComponent<Image>().color = Color.Lerp(emptyColor, fullColor, firstSlider.value / 5f); 
        } else {
            firstSlider.gameObject.SetActive(false);
        }
        if(gmanager.sliderID[1] > 0f) {

            secondSlider.gameObject.SetActive(true);
            secondSlider.value = gmanager.timerCalculation(gmanager.sliderID[1], 1);
            secondSlider.gameObject.transform.Find("Fill Area").Find("Fill").GetComponent<Image>().color = Color.Lerp(emptyColor, fullColor, secondSlider.value / 5f); 
        } else {
            secondSlider.gameObject.SetActive(false);
        }
        if(gmanager.sliderID[2] > 0f) {
            thirdSlider.gameObject.SetActive(true);
            thirdSlider.value = gmanager.timerCalculation(gmanager.sliderID[2], 2);
            thirdSlider.gameObject.transform.Find("Fill Area").Find("Fill").GetComponent<Image>().color = Color.Lerp(emptyColor, fullColor, thirdSlider.value / 5f); 
        } else {
            thirdSlider.gameObject.SetActive(false);
        }
    }

    public void whichPowerupText(int posicion, string powerup) {
        if(posicion == 0) {
            firstText.text = powerup;
        } else if(posicion == 1) {
            secondText.text = powerup;
        } else if(posicion == 2) {
            thirdText.text = powerup;
        }
    }
}
