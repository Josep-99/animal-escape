﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RestartButtonScript : MonoBehaviour
{
    private GameObject gameManager; 
    private Button button;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("GameManager");

        button = GetComponent<Button>();
        button.onClick.AddListener(this.ButtonClicked);
    }

    void ButtonClicked()
    {
        //avisamos game manager
        gameManager.GetComponent<GameManager>().restart = true;

        //Recargamos el nivel
        SceneManager.LoadScene("NivelProcedural");
    }
}
