﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SecondDeathScreenChange : MonoBehaviour
{
    private GameObject gameManager;
    private GameObject achievementManager;
    private Button button;
    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(this.ButtonClicked);

        gameManager = GameObject.Find("GameManager");
        achievementManager = GameObject.Find("AchievementManager");
    }

    void ButtonClicked()
    {
        achievementManager.GetComponent<AchievementManager>().checkIfNoCoinAchievement();
        gameManager.GetComponent<GameManager>().deathScreen1.SetActive(false);
        gameManager.GetComponent<GameManager>().deathScreen2.SetActive(true);
        GameObject totalCoinObject = GameObject.Find("TotalCoins");
        totalCoinObject.GetComponent<TotalCoinScript>().showTotal();
        GameObject reviveCoinsObject = GameObject.Find("UsedReviveCoins");
        reviveCoinsObject.GetComponent<ReviveTextScript>().updateText();
        GameObject achievementCoinsObject = GameObject.Find("AchievementCoins");
        achievementCoinsObject.GetComponent<AchievementTextScript>().updateText();
        GameObject obtainedCoins = GameObject.Find("ObtainedCoins");
        obtainedCoins.GetComponent<ObtainedCoinsScript>().updateText();
    }
}
