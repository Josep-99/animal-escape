﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReviveTextScript : MonoBehaviour
{
    Text text;
    int coinAmount;
    GameObject saveManager;

    public void updateText() {
        text = GetComponent<Text>();
        saveManager = GameObject.Find("SaveManager");
        coinAmount = saveManager.GetComponent<SaveManager>().reviveCoinsSpent;
        text.text = "Used revive coins: " + coinAmount.ToString();
    }
}
