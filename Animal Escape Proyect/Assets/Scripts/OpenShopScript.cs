﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OpenShopScript : MonoBehaviour
{
    private Button button;
    private GameObject shopmenu;
    private SaveManager saveManager;

    // Start is called before the first frame update
    void Start()
    {
        shopmenu = GameObject.Find("ShopCanvas");
        if (shopmenu != null) {
            //escondemos el canvas de shop
            shopmenu.SetActive(false);
        }
        saveManager = GameObject.Find("SaveManager").GetComponent<SaveManager>();
    }

    public void ButtonClicked()
    {
        //Recargamos el nivel
        Debug.Log("Tienda");
        //SceneManager.LoadScene("");
        shopmenu.SetActive(true);
    }
}
