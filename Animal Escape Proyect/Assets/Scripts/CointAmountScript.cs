﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CointAmountScript : MonoBehaviour
{
    int coinAmount;
    Text coinText;
    GameObject saveManager;
    // Start is called before the first frame update
    void Start()
    {
        coinText = GetComponent<Text>();
        saveManager = GameObject.Find("SaveManager");
        coinAmount = saveManager.GetComponent<SaveManager>().currentCoins;
        coinText.text = coinAmount.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        updateCoin();
    }

    void updateCoin() {
        coinAmount = saveManager.GetComponent<SaveManager>().currentCoins;
        coinText.text = "Coins: "+coinAmount.ToString();
    }
}
