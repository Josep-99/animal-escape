﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementManager : MonoBehaviour
{
    //Save manager
    private GameObject saveManager;
    //Primero de cada array es lo que se debe conseguir y el segundo las monedas obtenidas
    public int[,] distanceAchievementArray = new int[,]{{125, 50}, {250, 100}, {375, 200}, {500, 250}, {625, 300}, {750, 350}, {875, 400}, {1000, 450}};
    public int[,] gameCoinAchievementArray = new int[,]{{50, 20}, {100, 40}, {150, 60}, {200, 80}, {250, 100}, {300, 120}, {350, 140}, {400, 160}, {450, 180}, {500, 200}};
    public int[,] jumpAchievementArray = new int[,]{{20, 15}, {40, 50}, {60, 80}, {80, 120}, {100, 140}};
    public int[,] slideAchievementArray = new int[,]{{5, 10}, {10, 25}, {20, 75}, {35, 150}, {50, 250}};
    public int[,] reviveAchievementArray = new int[,]{{1, 150}, {2, 250}, {3, 350}, {4, 450}};
    public int noCoinReward = 100;
    
    //Variables para saber si la mision actual de un cierto tipo se ha completado durante la partida
    private bool currentCoinDone = false;
    private bool currentJumpDone = false;
    private bool currentSlideDone = false;
    public bool currentReviveDone = false;
    private bool currentDistanceDone = false;

    // Start is called before the first frame update
    void Start()
    {
        saveManager = GameObject.Find("SaveManager");
    }

    public void checkCoinAchievement(int current, int coins) {
        if(currentCoinDone == false) {
            if(hasNextAchievement(current, gameCoinAchievementArray)) {
            int neededCoins = gameCoinAchievementArray[current, 0];
                if(coins == neededCoins) {
                    int reward = gameCoinAchievementArray[current, 1];
                    /* Añadimos monedas, cambiamos la mision actual y bloqueamos la mision 
                       para no poder completar otra de este tipo durante la partida actual */
                    saveManager.GetComponent<SaveManager>().addCoins(reward);
                    current++;
                    saveManager.GetComponent<SaveManager>().increaseCoinAchievement(current);
                    currentCoinDone = true;
                    Debug.Log("Has completado la mision de recoger " + neededCoins + " monedas y obtenido " + reward + " monedas");
                }
            }
        }
    }

    public void checkIfObstacleJumpedAchievement(int current, int jumps) {
        if(currentJumpDone == false) {
            if(hasNextAchievement(current, jumpAchievementArray)) {
            int jumpsNeeded = jumpAchievementArray[current, 0];
                if(jumps == jumpsNeeded) {
                    int reward = jumpAchievementArray[current, 1];
                    /* Añadimos monedas, cambiamos la mision actual y bloqueamos la mision 
                       para no poder completar otra de este tipo durante la partida actual */
                    saveManager.GetComponent<SaveManager>().addCoins(reward);
                    current++;
                    saveManager.GetComponent<SaveManager>().increaseJumpAchievement(current);
                    currentJumpDone = true;
                    Debug.Log("Has completado la mision de saltar " + jumpsNeeded + " obstaculos y obtenido " + reward + " monedas");
                }
            }
        }
    }

    public void checkIfObstacleSlideAchievement(int current, int slides) {
        if(currentSlideDone == false) {
            if(hasNextAchievement(current, slideAchievementArray)) {
            int slidesNeeded = slideAchievementArray[current, 0];
                if(slides == slidesNeeded) {
                    int reward = slideAchievementArray[current, 1];
                    /* Añadimos monedas, cambiamos la mision actual y bloqueamos la mision 
                       para no poder completar otra de este tipo durante la partida actual */
                    saveManager.GetComponent<SaveManager>().addCoins(reward);
                    current++;
                    saveManager.GetComponent<SaveManager>().increaseSlideAchievement(current);
                    currentSlideDone = true;
                    Debug.Log("Has completado la mision de hacer " + slidesNeeded + " por debajo obstaculos y obtenido " + reward + " monedas");
                }
            }
        }
    }

    public void checkIfReviveAchievement(int current, int revives) {
        if(currentReviveDone == false) {
            if(hasNextAchievement(current, reviveAchievementArray)) {
            int revivesNeeded = reviveAchievementArray[current, 0];
                if(revives == revivesNeeded) {
                    int reward = reviveAchievementArray[current, 1];
                    /* Añadimos monedas, cambiamos la mision actual y bloqueamos la mision 
                       para no poder completar otra de este tipo durante la partida actual */
                    saveManager.GetComponent<SaveManager>().addCoins(reward);
                    current++;
                    saveManager.GetComponent<SaveManager>().increaseReviveAchievement(current);
                    currentReviveDone = true;
                    if(revivesNeeded == 1) {
                        Debug.Log("Has completado la mision de revivir " + revivesNeeded + " vez y obtenido " + reward + " monedas");
                    } else {
                        Debug.Log("Has completado la mision de revivir " + revivesNeeded + " veces y obtenido " + reward + " monedas");
                    }
                }
            }
        }
    }

    public void checkIfNoCoinAchievement() {
        if(saveManager.GetComponent<SaveManager>().currentCoins == 0 && saveManager.GetComponent<SaveManager>().noCoinGameAchievementComplete == 0) {
            saveManager.GetComponent<SaveManager>().addCoins(noCoinReward);
            Debug.Log("Has completado la mision de no recoger monedeas durante una partida y obtenido " + noCoinReward + " monedas");
            saveManager.GetComponent<SaveManager>().noCoinGameCompleted();
        }
    }

    public void checkIfDistanceAchievement(int current, float currentDistance) {
        currentDistance = currentDistance/5;
        if(currentDistanceDone == false) {
            if(hasNextAchievement(current, distanceAchievementArray)) {
                if(currentDistance >= (float)distanceAchievementArray[current, 0]) {
                    int reward = distanceAchievementArray[current, 1];
                    saveManager.GetComponent<SaveManager>().addCoins(reward);
                    Debug.Log("Has recorrido " + distanceAchievementArray[current, 0] + " y obtenido " + reward + " monedas");
                    current++;
                    saveManager.GetComponent<SaveManager>().increaseDistanceAchievement(current);
                    currentDistanceDone = true;
                }
            }
        }
    }

    public bool hasNextAchievement(int current, int[,] array) {
        if(current == array.GetLength(0)) {
            return false;
        } else {
            return true;
        }
    }
}
