﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContinuePriceScript : MonoBehaviour
{
    Text priceText;
    int price;
    GameObject gameManager;
    // Start is called before the first frame update
    public void updatePriceText()
    {
        priceText = GetComponent<Text>();
        gameManager = GameObject.Find("GameManager");
        price = gameManager.GetComponent<GameManager>().revivePrice;
        priceText.text = "Continue? " + price.ToString() + " coins";
    }
}