﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    public int coinCount;
    private int maxDistance;
    public string currentSkin;
    public int foxOwned;
    public int bearOwned;
    public int deerOwned;
    public int racoonOwned;
    public int rabbitOwned;
    public int wolfOwned;
    public int currentJumpAchievement;
    public int currentDistanceAchievement;
    public int currentGameCoinAchievement;
    public int currentSlideAchievement;
    public int currentReviveAchievement;
    public int noCoinGameAchievementComplete;
    //Variables usadas durante el juego
    public int currentCoins = 0;
    public int achievementTotalCoins = 0;
    public int reviveCoinsSpent = 0;
    //Managers
    private GameObject achievementManager;

    // Start is called before the first frame update
    void Start()
    {
        loadData();
        achievementManager = GameObject.Find("AchievementManager");
    }
    
    //Añade la moneda al total y a los obtenidas durante la partida
    public void addCoin() {
        coinCount = PlayerPrefs.GetInt("coins") + 1;
        PlayerPrefs.SetInt("coins", coinCount);
        currentCoins += 1;
        achievementManager.GetComponent<AchievementManager>().checkCoinAchievement(currentGameCoinAchievement, currentCoins);
    }

    //Suma una cantidad especifica de monedas
    public void addCoins(int achievementCoins) {
        coinCount = PlayerPrefs.GetInt("coins") + achievementCoins;
        achievementTotalCoins += achievementCoins;
        PlayerPrefs.SetInt("coins", coinCount);
        Debug.Log("Monedas totales de misiones esta partida: " + achievementTotalCoins);
    }

    //Quita una cantidad especifica de monedas
    public void substractReviveCoins(int coinToSubstract) {
        reviveCoinsSpent += coinToSubstract;
        coinCount = PlayerPrefs.GetInt("coins") - coinToSubstract;
        PlayerPrefs.SetInt("coins", coinCount);
    }

    //Devuelve el total de monedas
    public int getTotalCoins() {
        return PlayerPrefs.GetInt("coins");
    }

    //Funcion que guarda una skin como comprada
    public void purchasedSkin(string skinName) {
        string key = skinName + "owned";
        PlayerPrefs.SetInt(key, 1);
    }

    //Funciones que aumentan el id de la mision actual
    public void increaseCoinAchievement(int newMissionNumber) {
        PlayerPrefs.SetInt("currentgamecoinachievement", newMissionNumber);
    }
    public void increaseJumpAchievement(int newMissionNumber) {
        PlayerPrefs.SetInt("currentjumpachievement", newMissionNumber);
    }
    public void increaseSlideAchievement(int newMissionNumber) {
        PlayerPrefs.SetInt("currentslideachievement", newMissionNumber);
    }
    public void increaseReviveAchievement(int newMissionNumber) {
        PlayerPrefs.SetInt("currentreviveachievement", newMissionNumber);
    }
    public void increaseDistanceAchievement(int newMissionNumber) {
        PlayerPrefs.SetInt("currentdistanceachievement", newMissionNumber);
    }
    public void noCoinGameCompleted() {
        PlayerPrefs.SetInt("nocoingameachievement", 1);
    }

    //Elimina todos los datos de guardado, usar con precaucion
    /*private void deleteAll() {
        PlayerPrefs.DeleteAll();
    }*/

    //Carga los datos en variables, si no existen en el archivo de guardado los crea
    public void loadData() {
        if(PlayerPrefs.HasKey("coins") == false) {
            Debug.Log("Se crea la key 'coins'");
            PlayerPrefs.SetInt("coins", 0);
        } else {
            coinCount = PlayerPrefs.GetInt("coins");
        }
        if(PlayerPrefs.HasKey("maxdistance") == false) {
            Debug.Log("Se crea la key 'maxdistance'");
            PlayerPrefs.SetInt("maxdistance", 0);
        } else {
            maxDistance = PlayerPrefs.GetInt("maxdistance");
        }
        if(PlayerPrefs.HasKey("currentjumpachievement") == false) {
            Debug.Log("Es crea la key 'currentjumpachievement'");
            PlayerPrefs.SetInt("currentjumpachievement", 0);
        } else {
            currentJumpAchievement = PlayerPrefs.GetInt("currentjumpachievement");
        }
        if(PlayerPrefs.HasKey("currentdistanceachievement") == false) {
            Debug.Log("Es crea la key 'currentdistanceachievement'");
            PlayerPrefs.SetInt("currentdistanceachievement", 0);
        } else {
            currentDistanceAchievement = PlayerPrefs.GetInt("currentdistanceachievement");
        }
        if(PlayerPrefs.HasKey("currentgamecoinachievement") == false) {
            Debug.Log("Es crea la key 'currentgamecoinachievement'");
            PlayerPrefs.SetInt("currentgamecoinachievement", 0);
        } else {
            currentGameCoinAchievement = PlayerPrefs.GetInt("currentgamecoinachievement");
        }
        if(PlayerPrefs.HasKey("currentslideachievement") == false) {
            Debug.Log("Es crea la key 'currentslideachievement'");
            PlayerPrefs.SetInt("currentslideachievement", 0);
        } else {
            currentSlideAchievement = PlayerPrefs.GetInt("currentslideachievement");
        }
        if(PlayerPrefs.HasKey("currentreviveachievement") == false) {
            Debug.Log("Es crea la key 'currentreviveachievement'");
            PlayerPrefs.SetInt("currentreviveachievement", 0);
        } else {
            currentReviveAchievement = PlayerPrefs.GetInt("currentreviveachievement");
        }
        if(PlayerPrefs.HasKey("nocoingameachievement") == false) {
            Debug.Log("Es crea la key 'nocoingameachievement'");
            PlayerPrefs.SetInt("nocoingameachievement", 0);
        } else {
            noCoinGameAchievementComplete = PlayerPrefs.GetInt("nocoingameachievement");
        }
        if(PlayerPrefs.HasKey("currentskin") == false) {
            Debug.Log("Se crea la key 'currentskin'");
            PlayerPrefs.SetString("currentskin", "fox");
        } else {
            currentSkin = PlayerPrefs.GetString("currentskin");
        }
        if(PlayerPrefs.HasKey("foxowned") == false) {
            Debug.Log("Se crea la key 'foxowned'");
            PlayerPrefs.SetInt("foxowned", 1);
        } else {
            foxOwned = PlayerPrefs.GetInt("foxowned");
        }
        if(PlayerPrefs.HasKey("bearowned") == false) {
            Debug.Log("Se crea la key 'bearowned'");
            PlayerPrefs.SetInt("bearowned", 0);
        } else {
            bearOwned = PlayerPrefs.GetInt("bearowned");
        }
        if(PlayerPrefs.HasKey("deerowned") == false) {
            Debug.Log("Se crea la key 'deerowned'");
            PlayerPrefs.SetInt("deerowned", 0);
        } else {
            deerOwned = PlayerPrefs.GetInt("deerowned");
        }
        if(PlayerPrefs.HasKey("racoonowned") == false) {
            Debug.Log("Se crea la key 'racoonowned'");
            PlayerPrefs.SetInt("racoonowned", 0);
        } else {
            racoonOwned = PlayerPrefs.GetInt("racoonowned");
        }
        if(PlayerPrefs.HasKey("rabbitowned") == false) {
            Debug.Log("Se crea la key 'rabbitowned'");
            PlayerPrefs.SetInt("rabbitowned", 0);
        } else {
            rabbitOwned = PlayerPrefs.GetInt("rabbitowned");
        }
        if(PlayerPrefs.HasKey("wolfowned") == false) {
            Debug.Log("Se crea la key 'wolfowned'");
            PlayerPrefs.SetInt("wolfowned", 0);
        } else {
            wolfOwned = PlayerPrefs.GetInt("wolfowned");
        }
    }
    //metodo restar dinero al comprar
    public void substractCoins(int coins)
    {
        coinCount -= coins;
        PlayerPrefs.SetInt("coins", coinCount);
    }
    public void equipAnimal(string animal) 
    {
        currentSkin = animal;
        PlayerPrefs.SetString("currentskin", animal);
    }

    //metodo comprar animal
    public void buyAnimal(string animal)
    {
        if (animal.Equals("wolf"))
        {
            wolfOwned = 1;
            PlayerPrefs.SetInt("wolfowned", 1);
        }
        if (animal.Equals("racoon"))
        {
            racoonOwned = 1;
            PlayerPrefs.SetInt("racoonowned", 1);
        }
        if (animal.Equals("fox"))
        {
            foxOwned = 1;
            PlayerPrefs.SetInt("foxowned", 1);
        }
    }
}