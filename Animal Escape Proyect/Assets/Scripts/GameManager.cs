﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager gameManager;
    public ParticleSystem particulasInvensible;
    public ParticleSystem particulasSalto;
    GameObject Player;
    GameObject Enemy;
    PlayerController pcontroller;
    EnemicFollow eFollow;

    public bool inLife;
    public float speed = 10;
    public float distanceM;
    //velocidad al morir
    private float lastSpeed;
    //booleano que indica que han pagado para seguir
    public bool revive = false;
    //Variables que cambian cada que vez el jugador revive 
    public int reviveCount = 0;
    public int revivePrice = 150;
    //booleano reiniciar
    public bool restart = false;
    //canvas
    public GameObject deathScreen1;
    public GameObject deathScreen2;
    public GameObject revivePriceText;
    public int pauseFrame = 0;
    //Variables distancia
    public float initialPosition = 0f;
    public float currentPosition = 0f;
    public float currentDistance = 0f;
    //Otros managers
    private GameObject achievementManager;
    private GameObject saveManager;

    private PowerupUIScript powerupScript;

    //Estados de powerUps
    //Variables JumpPowerUp
    public bool isJumpPowerUpTakeIt = false;
    public bool isJumpingBool = false;
    public bool isJumpingP = false;

    private JumpingStates jumpStates = JumpingStates.Normal;

    private enum JumpingStates
    {
        Normal,
        isJumpingBooster,
        isJumpingMore,
        endJumpingBooster
    }

    private float newJumpForce = 12.5f;
    //Variables de temporizador
    private float isBoosterTime = 5f;
    private float capturarSegundosSalto = 0f;
    private float capturarSegundosIman = 0f;
    private float capturarSegundosInvenc = 0f;
    private int posicionSalto = 0;
    private int posicionIman = 0;
    private int posicionInvenc = 0;

    //Variables MagneticPowerUp
    public bool isMagneticPowerUpTakeIt = false;
    public bool isMagneticBool = false;

    private MagneticStates magneticStates = MagneticStates.Normal;

    private enum MagneticStates
    {
        Normal,
        isMagneticBooster,
        isMagneticAnimal,
        endMagneticBooster
    }

    //variables InveciblePowerUp
    public bool isInvenciblePowerUpTakeIt = false;
    public bool isInvencibleBool = false;

    private InvencibleStates invencibleStates = InvencibleStates.Normal;

    private enum InvencibleStates
    {
        Normal,
        isInvencibleBooster,
        isInvencibleSAnimal,
        endInvensibleBooster
    }

    public float[] sliderID = new float[] {0, 0, 0};

    void Awake()
    {
        if (gameManager == null)
        {
            gameManager = this;
            DontDestroyOnLoad(this);
            Debug.Log("GameManager active");
        }
        else
        {
            Destroy(this);
            Debug.Log("GameManager duplicate, delete this");
        }
    }

    void Start()
    {
        Player = GameObject.Find("Player");
        pcontroller = Player.GetComponent<PlayerController>();

        Enemy = GameObject.FindGameObjectWithTag("Enemy");
        eFollow = Enemy.GetComponent<EnemicFollow>();

        Enemy.transform.position = new Vector3(Enemy.transform.position.x, Enemy.transform.position.y, Player.transform.position.z - 9.5f);
    
        inLife = true;

        //deathScreen1 = GameObject.Find("DeathScreen1");
        deathScreen1.SetActive(false);
        //deathScreen2 = GameObject.Find("DeathScreen2");
        deathScreen2.SetActive(false);

        achievementManager = GameObject.Find("AchievementManager");
        saveManager = GameObject.Find("SaveManager");

        powerupScript = GameObject.Find("PowerupCanvas").GetComponent<PowerupUIScript>();

        jumpStates = JumpingStates.Normal;
        magneticStates = MagneticStates.Normal;
        invencibleStates = InvencibleStates.Normal;

        initialPosition = Player.transform.position.z;

        pcontroller.forwardspeed = speed;
        eFollow.forwardSpeed = speed;
    }

    // Update is called once per frame
    void Update()
    {
        if(lastSpeed > speed) {
            distanceM = currentDistance / 5;
            speed += 1f * Time.deltaTime;
        } else if (inLife) {
            distanceM = currentDistance / 5;
            speed += 0.15f * Time.deltaTime;
        }
        pcontroller.forwardspeed = speed;
        eFollow.forwardSpeed = speed;
        //pausa al morir
        if (!inLife) {
            //movemos enemigo
            Enemy.transform.position = new Vector3(Enemy.transform.position.x, Enemy.transform.position.y, Player.transform.position.z - 9.5f);
            if(pauseFrame >= 2) {
                PauseGame(true);
            } else {
                revivePriceText.GetComponent<ContinuePriceScript>().updatePriceText();
            }
            pauseFrame++;
        } else {
            PauseGame(false);
        }

        //revivir
        if (revive) {
            //guardamos la velocidad actual
            lastSpeed = speed;
            //Reestablecemos la velocidad
            speed = 10;
            //desactivamos canvas
            deathScreen1.SetActive(false);
            //desactivamos collider
            GameObject obstacle = Player.GetComponent<PlayerController>().obstaculoChoque;
            Destroy(obstacle);
            Player.GetComponent<PlayerController>().isBurned = false;
            //Quitamos pausa
            inLife = true;
            revive = false;
            pauseFrame = 0;
            reviveCount++;
            achievementManager.GetComponent<AchievementManager>().checkIfReviveAchievement(saveManager.GetComponent<SaveManager>().currentReviveAchievement, reviveCount);
            saveManager.GetComponent<SaveManager>().substractReviveCoins(revivePrice);
            revivePrice += 100;
        }

        //reiniciar
        if (restart) {
            /*Player = GameObject.Find("Player");
            pcontroller = Player.GetComponent<PlayerController>();

            Enemy = GameObject.FindGameObjectWithTag("Enemy");
            eFollow = Enemy.GetComponent<EnemicFollow>();

            inLife = true;

            deathScreen1 = GameObject.Find("DeathScreen1");
            deathScreen1.SetActive(false);
            deathScreen2 = GameObject.Find("DeathScreen2");
            deathScreen2.SetActive(false);
            restart = false;*/
            Destroy(this.gameObject);
        }

        jumpingHabilityUpdate();
        magneticHabilityUpdate();
        invencibleHabilityUpdate();

        if (isJumpPowerUpTakeIt)
        {
            isJumpPowerUpTakeIt = false;
            jumpStates = JumpingStates.isJumpingBooster;
        }

        if (isMagneticPowerUpTakeIt)
        {
            isMagneticPowerUpTakeIt = false;
            magneticStates = MagneticStates.isMagneticBooster;
        }

        if (isInvenciblePowerUpTakeIt)
        {
            isInvenciblePowerUpTakeIt = false;
            invencibleStates = InvencibleStates.isInvencibleBooster;
        }
 
        calculateDistance();
    }

    void PauseGame(bool inPause)
    {
        if (inPause)
        {
            Time.timeScale = 0;
        }
        else
        {
            //isPaused = false;
            pauseFrame = 0;
            Time.timeScale = 1;
        }
    }
    void jumpingHabilityUpdate()
    {
        switch (jumpStates)
        {
            case JumpingStates.Normal:
                break;

            case JumpingStates.isJumpingBooster:
                isJumping();
                break;

            case JumpingStates.isJumpingMore:
                isJumpingMore();
                break;

            case JumpingStates.endJumpingBooster:
                isEndingBooster();
                break;
            default:
                jumpStates = JumpingStates.Normal;
                break;
        }
    }

    private void isJumping()
    {
        capturarSegundosSalto = Time.time;
        isJumpingBool = true;
        particulasSalto.Play();
        pcontroller.jumpForce = newJumpForce;
        nextJumpingState();
        if(posicionSalto == 0) {
            if(sliderID[0] == 0) {
                sliderID[0] = capturarSegundosSalto;
                posicionSalto = 1;
                powerupScript.whichPowerupText(0, "Jump");
            } else if(sliderID[1] == 0) {
                sliderID[1] = capturarSegundosSalto;
                posicionSalto = 2;
                powerupScript.whichPowerupText(1, "Jump");
            } else if(sliderID[2] == 0) {
                sliderID[2] = capturarSegundosSalto;
                posicionSalto = 3;
                powerupScript.whichPowerupText(2, "Jump");
            }
        } else {
            sliderID[posicionSalto - 1] = Time.time;
        }
        
    }

    private void isJumpingMore()
    {
        if (Time.time > capturarSegundosSalto + isBoosterTime)
        {
            nextJumpingState();
        }
    }

    private void isEndingBooster()
    {
        pcontroller.jumpForce = 10f;
        particulasSalto.Stop();
        isJumpingBool = false;
        nextJumpingState();
        posicionSalto = 0;
    }

    void nextJumpingState()
    {
        if (jumpStates == JumpingStates.Normal)
        {
            jumpStates = 0;
        }
        else
        {
            jumpStates++;
        }
    }


    void magneticHabilityUpdate()
    {
        switch (magneticStates)
        {
            case MagneticStates.Normal:
                break;

            case MagneticStates.isMagneticBooster:
                isMagnetic();
                break;

            case MagneticStates.isMagneticAnimal:
                isMagneticAnimal();
                break;

            case MagneticStates.endMagneticBooster:
                isEndingBoosterMagnetic();
                break;

            default:
                magneticStates = MagneticStates.Normal;
                break;
        }
    }
    private void isMagnetic()
    {
        capturarSegundosIman = Time.time;
        isMagneticBool = true;
        nextMagneticState();
        if(posicionIman == 0) {
            if(sliderID[0] == 0) {
                sliderID[0] = capturarSegundosIman;
                posicionIman = 1;
                powerupScript.whichPowerupText(0, "Magnet");
            } else if(sliderID[1] == 0) {
                sliderID[1] = capturarSegundosIman;
                posicionIman = 2;
                powerupScript.whichPowerupText(1, "Magnet");
            } else if(sliderID[2] == 0) {
                sliderID[2] = capturarSegundosIman;
                posicionIman = 3;
                powerupScript.whichPowerupText(2, "Magnet");
            }
        } else {
            sliderID[posicionIman - 1] = Time.time;
        }
    }

    private void isMagneticAnimal()
    {
        if (Time.time > capturarSegundosIman + isBoosterTime)
        {
            nextMagneticState();
        }
    }

    private void isEndingBoosterMagnetic()
    {
        isMagneticBool = false;
        nextMagneticState();
        posicionIman = 0;
    }

    void nextMagneticState()
    {
        if (magneticStates == MagneticStates.Normal)
        {
            magneticStates = 0;
        }
        else
        {
            magneticStates++;
        }
    }


    void invencibleHabilityUpdate()
    {
        switch (invencibleStates)
        {
            case InvencibleStates.Normal:
                break;

            case InvencibleStates.isInvencibleBooster:
                Debug.Log("1");
                isInvensible();
                break;

            case InvencibleStates.isInvencibleSAnimal:
                Debug.Log("2");
                isinvensibleAnimal();
                break;

            case InvencibleStates.endInvensibleBooster:
                Debug.Log("3");
                isEndingBoosterInvensible();
                break;

            default:
                invencibleStates = InvencibleStates.Normal;
                break;
        }
    }
    private void isInvensible()
    {
        capturarSegundosInvenc = Time.time;
        particulasInvensible.Play();
        isInvencibleBool = true;
        nextInvencibleState();
        if(posicionInvenc == 0) {
            if(sliderID[0] == 0) {
                sliderID[0] = capturarSegundosInvenc;
                powerupScript.whichPowerupText(0, "Invincibility");
                posicionInvenc = 1;
            } else if(sliderID[1] == 0) {
                sliderID[1] = capturarSegundosInvenc;
                powerupScript.whichPowerupText(0, "Invincibility");
                posicionInvenc = 2;
            } else if(sliderID[2] == 0) {
                sliderID[2] = capturarSegundosInvenc;
                powerupScript.whichPowerupText(0, "Invincibility");
                posicionInvenc = 3;
            }
        } else {
            sliderID[posicionInvenc - 1] = Time.time;
        }
    }

    private void isinvensibleAnimal()
    {
        if (Time.time > capturarSegundosInvenc + isBoosterTime)
        {
            nextInvencibleState();
        }
    }

    private void isEndingBoosterInvensible()
    {
        isInvencibleBool = false;
        particulasInvensible.Stop();
        nextInvencibleState();
        posicionInvenc = 0;
    }

    void nextInvencibleState()
    {
        if (invencibleStates == InvencibleStates.Normal)
        {
            invencibleStates = 0;
        }
        else
        {
            invencibleStates++;
        }
    }


    private void calculateDistance() {
        currentPosition = Player.transform.position.z;
        currentDistance = currentPosition - initialPosition;
        achievementManager.GetComponent<AchievementManager>()
        .checkIfDistanceAchievement(
            saveManager.GetComponent<SaveManager>().currentDistanceAchievement, currentDistance);
    }

    public float timerCalculation(float tiempoPasado, int position) {
        float timeLeft = (tiempoPasado + isBoosterTime) - Time.time;
        if(timeLeft <= 0) {
            sliderID[position] = 0;
        }
        return timeLeft;
    }
}
