﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObtainedCoinsScript : MonoBehaviour
{
    Text coinText;
    int coinAmount;
    GameObject saveManager;
    // Start is called before the first frame update
    public void updateText()
    {
        coinText = GetComponent<Text>();
        saveManager = GameObject.Find("SaveManager");
        coinAmount = saveManager.GetComponent<SaveManager>().currentCoins;
        coinText.text = "Obtained coins: " + coinAmount.ToString();
    }
}