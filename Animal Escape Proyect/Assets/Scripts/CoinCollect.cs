﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CoinCollect : MonoBehaviour
{
    private Transform Player;
    public Material colorInicial;
    public Material colorMagnetico;
    public ParticleSystem particulasMonedas;
    GameObject GameManager;
    GameManager gmanager;

    GameObject P;
    PlayerController pcontroller;

    public bool monedaYendo = false;
    public bool isMagnetic;
    public bool isJumping;
    public bool isSliding;

    void Start()
    {
        GetComponent<Renderer>().material = colorInicial;
        Player = GameObject.Find("Player").GetComponent<Transform>();
        GameManager = GameObject.Find("GameManager");
        gmanager = GameManager.GetComponent<GameManager>();

        P = GameObject.Find("Player");
        pcontroller = P.GetComponent<PlayerController>();

    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player") {
            particulasMonedas.Play();
            FindObjectOfType<SoundManager>().PlaySound("coinPick");
            GameObject saveManager = GameObject.Find("SaveManager");
            saveManager.GetComponent<SaveManager>().addCoin();
            gameObject.GetComponent<Renderer>().enabled = false;
           

        }
    }
    //Rotacion de la moneda
    void Update()
    {
        isMagnetic = gmanager.isMagneticBool;
        isJumping = gmanager.isJumpingBool;
        isSliding = pcontroller.isSliding;

        transform.Rotate(0, 90 * Time.deltaTime, 0);
        if (isJumping)
        {
            if (Player.transform.position.y > 1.150)
            {
                if (Vector3.Distance(transform.position, Player.position) < 3.5)
                {
                    transform.position = Vector3.MoveTowards(transform.position, Player.transform.position + Vector3.forward * 1f, 3f);
                }
                
            }
        }
        if (!isMagnetic)
        {
            if (monedaYendo)
            {
                GetComponent<Renderer>().material = colorInicial;
                transform.position = Vector3.MoveTowards(transform.position, Player.transform.position + Vector3.forward * 1f, 3f);

            }
            else
            {
                GetComponent<Renderer>().material = colorInicial;
            }
        }
        if (isMagnetic)
        {
            GetComponent<Renderer>().material = colorMagnetico;

            if (isSliding)
            {
                if (Vector3.Distance(transform.position, Player.position) < 7)
                {
                    monedaYendo = true;
                    transform.position = Vector3.MoveTowards(transform.position, Player.transform.position, 3f);
                }
                else
                {
                    monedaYendo = false;
                }
            }
            else
            {
                if (Vector3.Distance(transform.position, Player.position) < 7)
                {
                    monedaYendo = true;
                    transform.position = Vector3.MoveTowards(transform.position, Player.transform.position + Vector3.forward * 1f, 3f);
                }
                else
                {
                    monedaYendo = false;
                }
            }
        }
    }
}
